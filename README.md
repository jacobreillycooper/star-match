# Project - Star Match

The project is currently in the development stage, the game can be played but under no time constraint or pressure. 

## Research to take from the project

1. Hooks

    a) Side effect hooks

    b) Where and when to use them

2. What makes a component - size, reusability etc?

## Styling

The CSS has been provided by the developer in the tutorial I am following. 

## Development

Part of the #100daysOfCode challenge - I will be revisiting it upon completition to test my knowledge and to comment throughout.